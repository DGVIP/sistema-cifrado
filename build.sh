#!/usr/bin/env bash

# For the front end

cd client
yarn
yarn build


# for the backend
cd .. && cd server
yarn
yarn start
