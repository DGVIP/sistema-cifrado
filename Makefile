front: client
	cd client && yarn && yarn build
back: front server
	cd server && yarn && yarn start
