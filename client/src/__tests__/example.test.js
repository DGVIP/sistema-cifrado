test("Test 1", function () {
   expect(1 + 2).toBe(3);
});
test("Test 2", function () {
   expect(5 - 4).toBe(1);
});
test("Test 3", function () {
   expect("a" + "b").toBe("ab");
});
test("Test 4", function () {
   expect(4 % 2).toBe(0);
});
test("Test 5", function () {
   expect("4" + 4).toBe("44");
});
